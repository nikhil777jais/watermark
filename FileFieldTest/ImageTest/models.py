from django.db import models

# Create your models here.
class Image(models.Model):
  title = models.CharField(max_length=200)
  profile_pic = models.ImageField(upload_to='profile/%m')

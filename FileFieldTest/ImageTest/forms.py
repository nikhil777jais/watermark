from django import forms
from django.db import models
from django.forms import fields
from . models import Image

class ImageInputForm(forms.ModelForm):
  class Meta:
    model = Image
    fields = ('title', 'profile_pic')
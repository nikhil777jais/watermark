from rest_framework.response import Response
from ImageTest.models import Image
from django.shortcuts import render
from . forms import ImageInputForm

from rest_framework.views import APIView
from rest_framework import status
from . serializer import ImageSerializer
from .watermark import add_watermark

# Create your views here.
def index(request):
  if request.method == 'POST':
    fm = ImageInputForm(request.POST, request.FILES)
    if fm.is_valid:
      fm.save()
      img_obj = fm.instance
      return render(request, 'ImageTest/index.html', {'form': fm, 'img_obj': img_obj})  
  else:
    fm = ImageInputForm()
  return render(request, 'ImageTest/index.html', {'form': fm})
  
class ImageAPI(APIView):
  def get(self, request, id=None, format=None):
    if id != None:
      img_obj = Image.objects.get(pk=id)
      serializer = ImageSerializer(img_obj)
      return Response({"payload":serializer.data, "msg":"data retrived"}, status=status.HTTP_200_OK)
    img_obj = Image.objects.all()
    serializer = ImageSerializer(img_obj, many=True)
    return Response({"payload":serializer.data, "msg":"data retrived"}, status=status.HTTP_200_OK)

  def post(self, request, format=None):
    serializer = ImageSerializer(data= request.data)
    if serializer.is_valid():
      serializer.save()
      add_watermark(serializer.data['profile_pic'])
      return Response({"payload":serializer.data, "msg":"data saved"}, status=status.HTTP_201_CREATED)
    return Response({"payload":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)













from PIL import Image, ImageDraw, ImageFont
from pathlib import Path

#Create an Image Object from an Image
def add_watermark(image):
  myimage = 'ImageTest'+image
  im = Image.open(myimage)
  width, height = im.size

  draw = ImageDraw.Draw(im)
  text = "@symbtechnologies.com"

  font = ImageFont.truetype('calibril.ttf', 16)
  textwidth, textheight = draw.textsize(text, font)

  # calculate the x,y coordinates of the text
  margin = 10
  x = width - textwidth - margin
  y = height - textheight - margin
  # draw watermark in the bottom right corner
  # draw.text((x, y), text, font=font)
  draw.text((x, y), text, font=font, fill=(255,255,255,128))
  #Save watermarked image
  print(myimage)
  im.save(myimage)  

# img = "trump.jpg"
# add_watermark(img)
